# Documentation

## Example Playbook

```
---
- name: Install Ansible
  hosts: all
  remote_user: root
  become: yes

  tasks: 
  - name: Install Ansible Fedora // RHEL on Control node.
    yum:
      name: epel-release
      state: latest
    yum:
      name: ansible
      state: latest

